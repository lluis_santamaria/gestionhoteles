﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeopleLib
{
    public class CPilaDeshacer
    {
        const int MAX = 25;
        public CPersona[] stackData = new CPersona[MAX];
        public int topStack;


        public CPilaDeshacer()
        {
            topStack = -1;
        }

        //metodos para operar con la pila de acciones de deshacer

        public bool is_empty()
        {
            if (topStack == -1)
                return true;
            else
                return false;
        }
        public bool is_full()
        {
            if (topStack == MAX - 1)
                return true;
            else
                return false;
        }



        public void push(CPersona p)
        {
            topStack = topStack + 1;
            stackData[topStack] = p;
        }

        public CPersona pop()
        //borra el objeto CPersona de encima de la pila y lo saca
        {
            topStack = topStack - 1;
            return stackData[topStack + 1];
            
        }

        public CPersona top()
        //devuelve el objeto CPersona de encima de la pila sin sacarlo
        {
            return stackData[topStack];
        }
  






    }
}
