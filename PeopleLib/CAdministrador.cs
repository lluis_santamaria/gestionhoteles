﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeopleLib
{
    public class CAdministrador
    {
        string contrasenya;
        string nombre;

        public CAdministrador()
        {
        }
        public CAdministrador(string D)
        {
            contrasenya=D;   
        }
        public void asignar_nombre(string n)
        {
            nombre = n;
        }
        public string obtener_nombre()
        {
            return nombre;
        }
        public void asignar_contrasenya(string d)
        {
            contrasenya = d;
        }
        public string obtener_contrasenya()
        {
            return contrasenya;
        }
    }
}
