﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace PeopleLib
{
    
    public class CListaHoteles
    {
        const int MAX = 10;
        
        
        CHotel []lista_hoteles= new CHotel[MAX];
        int numero;


        public CHotel obtener_hotel(int index)
        {
            return lista_hoteles[index];
        }

        public void asignar_hotel(CHotel h ,int n)
        {
            lista_hoteles[n] = h;
        }
        public void eliminar_hotel(int x)
        {
            lista_hoteles[x] = null;
        }

        public int obtener_numero()
        {
            return numero;
        }
        public void asignar_numero(int y)
        {
            numero = y;
        }

        public int cargar(string nombre_fichero)
        {
            StreamReader F;
            string linea_cliente;
            int i = 0;

            string[] datos_cliente = new string[5];

            try
            {
                F = new StreamReader(nombre_fichero);

            }
            catch (FileNotFoundException)
            {
                return -1;
            }

            linea_cliente = F.ReadLine();

            while (linea_cliente != null && i < MAX - 1)
            {
                if (linea_cliente == "")
                {
                    F.Close();
                    return -2;
                }
                lista_hoteles[i] = new CHotel();

                lista_hoteles[i].set_identificador(linea_cliente);

                linea_cliente = F.ReadLine();

                while (linea_cliente != "--------")
                {
                    try
                    {
                        datos_cliente = linea_cliente.Split(',');
                    }
                    catch (IndexOutOfRangeException)
                    {
                        F.Close();
                        return -2;
                    }

                    try
                    {
                        lista_hoteles[i].set_persona(new CPersona(datos_cliente[4], datos_cliente[2], Convert.ToInt32(datos_cliente[3])), Convert.ToInt32(datos_cliente[0]), Convert.ToInt32(datos_cliente[1]));
                    }
                    catch
                    {
                        F.Close();
                        return -2;
                    }

                    linea_cliente = F.ReadLine();
                }
                lista_hoteles[i].set_balance(lista_hoteles[i].calcular_balance());
                i++;
                linea_cliente = F.ReadLine();
            }
            F.Close();
            numero = i;
            return 0;

        }

        public int guardar(string nombre_fichero)
        //retorna 0 si ha ido bien y retorna -1 en caso de error
        {
            try
            {
                StreamWriter F = new StreamWriter(nombre_fichero);

                int i = 0, j = 0, k = 0;
                while (k < numero)
                {
                    F.WriteLine(lista_hoteles[k].get_identificador());


                    while (i < lista_hoteles[k].get_matriz_personas().GetLength(0))
                    {
                        while (j < lista_hoteles[k].get_matriz_personas().GetLength(1))
                        {
                            if (lista_hoteles[k].get_persona(i, j) != null)
                            {
                                F.WriteLine(i + "," + j + "," + lista_hoteles[k].get_persona(i, j).get_DNI() + "," + lista_hoteles[k].get_persona(i, j).get_edad() + "," + lista_hoteles[k].get_persona(i, j).get_nombre());
                            }

                            j++;
                        }

                        j = 0;
                        i++;
                    }
                    i = 0;
                    F.WriteLine("--------");
                    k++;
                }
                F.Close();
                return 0;
            }
            catch
            {
                return -1;
            }
        }
    }
}
