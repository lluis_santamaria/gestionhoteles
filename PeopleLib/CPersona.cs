﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeopleLib
{
    //implementamos la clase CPersona a IClonable
    public class CPersona:ICloneable
    {
        string nombre;
        string DNI;
        int edad;
        string DNI_titular;
        //la propiedad DNI_titular identifica al cliente que ha hecho la reserva de este huésped.Es útil
        //para poder listar las reservas que ha hecho un cliente determinado

        //métodos para tratar con objetos tipo CPersona

        //constructores
        
     
        
        public CPersona()
        {
            DNI_titular = "";
        }
        public CPersona(string n, string D, int e)
        {
            nombre = n;
            DNI = D;
            edad = e;
            DNI_titular = "";
        }

        //constructor de copia
        public CPersona(CPersona p)
        {
            this.DNI = p.DNI;
            this.nombre = p.nombre;
            this.edad = p.edad;
        }
        //Propiedades, permiten realizar encapsulación de los atributos sin tener que utilizar funciones get/set
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Dni
        {
            get { return DNI; }
            set { DNI = value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        //La función retorna un Object genérico
        public Object Clone()
        {
            return this.MemberwiseClone();
        }

        //métodos get para obtener el valor de una propiedad de un objeto CPersona
        public string get_nombre()
        {
            return nombre;
        }
        public string get_DNI()
        {
            return DNI;
        }
        public int get_edad()
        {
            return edad;
        }

        //métodos set para cambiar el valor de una propiedad de un objeto CPersona
        public void set_nombre(string n)
        {
            nombre = n;
        }
        public void set_DNI(string D)
        {
            DNI = D;
        }
        public void set_edad(int e)
        {
            edad = e;
        }
        public void set_DNI_titular(string dni)
        {
            DNI_titular = dni;
        }
        public string get_DNI_titular()
        {
            return DNI_titular;
        }
    }
}
