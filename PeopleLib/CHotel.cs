﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PeopleLib
{
    public class CHotel
    {
        const int MAXi = 5;
        const int MAXj = 5;
        string identificador;
        float balance;
        CPersona[,] matriz_personas = new CPersona[MAXi, MAXj];


        public CPersona get_persona(int i, int j)
        //devuelve el objeto CPersona almacenado en la posición [i,j]
        {
            return matriz_personas[i, j];
        }

        public string get_identificador()
        {
            return identificador;
        }

        public float get_balance()
        {
            return balance;
        }

        public int get_MAXi()
        {
            return MAXi;
        }
        public int get_MAXj()
        {
            return MAXj;
        }

        public CPersona[,] get_matriz_personas()
        {
            //creamos una matriz nueva a la que le asignamos los valores y propiedades de m manualmente(valor por valor)
            //con la finalidad de pasar la matriz de personas por valor y no por referencia. (en C#
            //sólo los tipos básicos se pasan por valor).
            CPersona[,] matriz_personas2 = new CPersona[matriz_personas.GetLength(0), matriz_personas.GetLength(1)];
            int i = 0, j = 0;
            while (i < matriz_personas.GetLength(0))
            {
                while (j < matriz_personas.GetLength(1))
                {
                    if (matriz_personas[i, j] != null)
                    {
                        matriz_personas2[i, j] = new CPersona();
                        matriz_personas2[i, j].set_nombre(matriz_personas[i, j].get_nombre());
                        matriz_personas2[i, j].set_DNI(matriz_personas[i, j].get_DNI());
                        matriz_personas2[i, j].set_edad(matriz_personas[i, j].get_edad());
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            return matriz_personas2;
        }

        //métodos set para cambiar los valores de un objeto tipo CHotel
        public void set_persona(CPersona p, int i, int j)
        {
            matriz_personas[i, j] = p;
        }

        public void set_identificador(string i)
        {
            identificador = i;
        }

        public void set_balance(float b)
        {
            balance = b;
        }

        public void set_matriz_personas(CPersona[,] m)
        {
            matriz_personas = m;
        }

        public int cargar(string nombre_fichero)
        //creamos la función para cargar los datos del fichero de texto cuyo nombre es nombre_fichero. Si la operación
        //ha ido bien devuelve 0, si no ha encontrado el fichero -1, y si los datos del fichero no tienen el formato
        //correcto -2
        {
            int i = 0, j = 0;

            while (i < MAXi)
            //recorremos la matriz eliminando sus elementos CPersona actuales para que no interfieran con la posterior carga.
            {
                while (j < MAXj)
                {
                    set_persona(null, i, j);
                    j++;
                }
                j = 0;
                i++;
            }
            StreamReader F;
            string linea_cliente;



            string[] datos_cliente = new string[5];

            try
            {
                F = new StreamReader(nombre_fichero);

            }
            catch (FileNotFoundException)
            {
                return -1;
            }


            set_identificador(F.ReadLine());


            if (get_identificador() == "")
            {
                F.Close();
                return -2;
            }

            linea_cliente = F.ReadLine();

            while (linea_cliente != null)
            {
                

                try
                {
                    datos_cliente = linea_cliente.Split(',');
                }

                catch (IndexOutOfRangeException)
                {
                    F.Close();
                    return -2;
                }



                try
                {
                    set_persona(new CPersona(datos_cliente[4], datos_cliente[2], Convert.ToInt32(datos_cliente[3])), Convert.ToInt32(datos_cliente[0]), Convert.ToInt32(datos_cliente[1]));
                }


                catch
                {
                    F.Close();
                    return -2;
                }

                linea_cliente = F.ReadLine();

            }

            F.Close();
            set_balance(calcular_balance());
            return 0;
        }

        public float calcular_balance()
        //esta funcion hará el balance de la ocupacion del hotel y éste se actualizará 
        {// cada vez que modifiquemos los datos del hotel al invocar la susodicha

            int i = 0, j = 0, contador = 0;
            while (i < matriz_personas.GetLength(0))
            {
                while (j < matriz_personas.GetLength(1))
                {
                    if (get_persona(i, j) != null)
                    {
                        contador++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            return contador * 100 / (matriz_personas.GetLength(0) * matriz_personas.GetLength(1));
        }

        public void guardar(string nombre_fichero)
        //esta es la función para guardar los datos al fichero de texto cuyo nombre es nombre_fichero desde la estructura de datos CHotel 
        {
            StreamWriter F;
            F = new StreamWriter(nombre_fichero);

            F.WriteLine(get_identificador());

            int i = 0, j = 0;

            while (i < MAXi)
            {
                while (j < MAXj)
                {
                    if (get_persona(i, j) != null)
                    {
                        F.WriteLine(i + "," + j + "," + get_persona(i, j).get_DNI() + "," + get_persona(i, j).get_edad() + "," + get_persona(i, j).get_nombre());
                    }
                    j++;
                }

                j = 0;
                i++;

            }

            F.Close();

        }

        public void asignar(CPersona nuevo_cliente, int planta, int habitacion)
        //Esta función asigna un nuevo cliente a la posición [planta,habitación] de la matriz del objeto hotel
        {
            set_persona(nuevo_cliente, planta, habitacion);
            set_balance(calcular_balance());
        }
        public CPersona buscar_cliente_nombre(string nombre)
        //este método nos devuelve el objeto CPersona cuyo nombre es nombre en el hotel.
        {
            int i = 0, j = 0;
            Boolean encontrado = false;

            while (i < matriz_personas.GetLength(0)&&!encontrado)
            {
                while (j < matriz_personas.GetLength(1) && !encontrado)
                {
                    if (matriz_personas[i, j] != null)
                    {
                        if (matriz_personas[i, j].get_nombre() == nombre)
                            encontrado = true;
                    }
                    if (!encontrado)
                        j++;
                }
                if (!encontrado)
                {
                    j = 0;
                    i++;
                }
            }
           
            if (encontrado)
            {
                return matriz_personas[i, j];
            }
            else
                return null;
        }

        
        public CPersona buscar_cliente_DNI(string DNI)
        //este método nos devuelve el objeto CPersona cuyo nombre es nombre en el hotel.
        {
            int i = 0, j = 0;
            Boolean encontrado = false;

            while (i < matriz_personas.GetLength(0) && !encontrado)
            {
                while (j < matriz_personas.GetLength(1) && !encontrado)
                {
                    if (matriz_personas[i, j] != null)
                    {
                        if (matriz_personas[i, j].get_DNI() == DNI)
                            encontrado = true;
                    }
                    if (!encontrado)
                        j++;
                }
                if (!encontrado)
                {
                    j = 0;
                    i++;
                }
            }
            if (encontrado)
            {
                return matriz_personas[i, j];
            }
            else
                return null;
        }

        public CPersona buscar_cliente_edad(int edad)
        //este método nos devuelve el objeto CPersona en el hotel cuya edad es el parámetro edad.
        {
            int i = 0, j = 0;
            Boolean encontrado = false;

            while (i < matriz_personas.GetLength(0) && !encontrado)
            {
                while (j < matriz_personas.GetLength(1) && !encontrado)
                {
                    if (matriz_personas[i, j] != null)
                    {
                        if (matriz_personas[i, j].get_edad() == edad)
                            encontrado = true;
                    }
                    if (!encontrado)
                        j++;
                }
                if (!encontrado)
                {
                    j = 0;
                    i++;
                }
            }
            if (encontrado)
            {
                return matriz_personas[i, j];
            }
            else
                return null;
        }

        public int buscar(CPersona cli, ref int planta, ref int habitacion)
        //Esta función busca por referencia la persona cli que ocupa la posición [planta,habitación] en la matriz del objeto hotel
        //devuelve 0 si encuentra la persona, y devuelve -1 si no la encuentra
        {
            if (get_persona(planta, habitacion) == cli)
                return 0;
            else
                return -1;
        }

        public int eliminar(string baja_cliente)
        //esta función elimina un objeto persona cuya propiedad nombre se corresponde con baja_cliente
        {
            Boolean encontrado;
            encontrado = false;
            int i = 0, j = 0;

            while (i < matriz_personas.GetLength(0) && !encontrado)
            {
                while (j < matriz_personas.GetLength(1) && !encontrado)
                {
                    if (get_persona(i, j) != null)
                    {
                        if (get_persona(i, j).get_nombre() == baja_cliente)
                        {
                            encontrado = true;
                        }
                        else
                        {
                            j++;
                        }
                    }
                    else
                    {
                        j++;
                    }
                }
                if (!encontrado)
                {
                    i++;
                    j = 0;
                }
            }

            if (encontrado)
            {
                set_persona(null, i, j);
                set_balance(calcular_balance());
                return 0;
            }
            else
            {
                return -1;

            }
        }

        public void vaciar(int planta, int hab)
        //esta función asigna valor null la posición [planta,hab] de la matriz de personas del hotel
        {
            matriz_personas[planta, hab] = null;
        }

        public CHotel copy()
        //este método retorna una copia del hotel sin referenciar, sino como objeto independiente por valor.
        {
            CHotel copy = new CHotel();
            copy.set_identificador(get_identificador());
            copy.set_balance(get_balance());

            CPersona[,] matriz_personas2 = new CPersona[MAXi, MAXj];
            int i = 0, j = 0;
            while (i < MAXi)
            {
                while (j < MAXj)
                {
                    if (matriz_personas[i, j] != null)
                    {
                        matriz_personas2[i, j] = new CPersona();
                        matriz_personas2[i, j].set_nombre(matriz_personas[i, j].get_nombre());
                        matriz_personas2[i, j].set_DNI(matriz_personas[i, j].get_DNI());
                        matriz_personas2[i, j].set_edad(matriz_personas[i, j].get_edad());
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            copy.set_matriz_personas(matriz_personas2);
            return copy;
        }
    }
}