﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeopleLib
{
    public class CCliente
    {
        string DNI;
        string nombre_usuario;

        public CCliente()
        {
        }
        public CCliente(string D)
        {
            DNI=D;   
        }
        public void asignar_nombre_usuario(string n)
        {
            nombre_usuario = n;
        }
        public string obtener_nombre_usuario()
        {
            return nombre_usuario;
        }
        public void asignar_DNI(string d)
        {
            DNI = d;
        }
        public string obtener_DNI()
        {
            return DNI;
        }
    }
}
