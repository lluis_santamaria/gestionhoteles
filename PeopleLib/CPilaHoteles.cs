﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeopleLib
{
    public class CPilaHoteles
    {      
        const int MAX = 1000;
        public CHotel[] stackData = new CHotel[MAX];
        public int topStack;

        //se ha omitido el método inicializador porque topStack ya se inicializa a -1 en el método constructor

        //constructor de la pila
        public CPilaHoteles()
        {
            topStack = -1;
        }
        
        //métodos para operar con CPilaHoteles
        public bool is_empty()
        {
            if (topStack == -1)
                return true;
            else
                return false;
        }
        public bool is_full()
        {
            if (topStack == MAX - 1)
                return true;
            else
                return false;
        }
        public void push(CHotel e)
        {
            topStack = topStack + 1;
            stackData[topStack] = e;
        }
        public CHotel pop()
        //devuelve el hotel de encima de la pila y lo saca de ésta.
        {
            topStack = topStack - 1;
            return stackData[topStack + 1];
        }
        public CHotel top()
        //devuelve el hotel de encima de la pila sin sacarlo de la pila.
        {
            return stackData[topStack];
        }
    }
}
