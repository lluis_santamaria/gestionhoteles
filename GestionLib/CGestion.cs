﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.Data.Common;


namespace GestionLib
{
    public class CGestion
    {
        private SQLiteConnection cnx;

        public void Iniciar()
        {
            string dataSource = "Data Source= C:\\Users\\CARDONA\\Desktop\\Base_p2\\datos.db";
            cnx = new SQLiteConnection(dataSource);
            cnx.Open();
        }
        public void Cerrar()
        {
            cnx.Close();
        }
        public DataSet GetClientes()
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT * FROM clientes;";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "clientes");
            return ds;
        }

        public DataSet GetAdministradores()
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT * FROM administradores;";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "administradores");
            return ds;
        }

        public DataSet GetClientesSinContrasenya()
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT nombre_usuario, nombre, DNI, telefono, data_naixement, email, direccion_postal, numero_tarjeta FROM clientes;";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "clientes");
            return ds;
        }

        public int Añadir_cliente(string nombre_usuario,string nombre,string DNI,int telefono,string fecha_nacimiento,string contrasenya,string email,string direccion_postal,string numero_tarjeta)
        {
            string sql;
            sql = "INSERT into clientes values ('"+nombre_usuario+"', '"+nombre+"', '"+DNI+"', "+telefono+", '"+fecha_nacimiento+"', '"+contrasenya+"', '"+email+"', '"+direccion_postal+"', '"+numero_tarjeta+"');";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAfected = command.ExecuteNonQuery();
            return rowsAfected;
        }
        public int Añadir_administrador(string nombre_administrador, string contrasenya)
        //Éste método añade un administrador a la tabla de administradores
        {
            string sql;
            sql = "INSERT into administradores values ('" + nombre_administrador + "', '" + contrasenya + "');";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAfected = command.ExecuteNonQuery();
            return rowsAfected;
        }

        public int ponNombre_usuario(string DNI, string nombre_usuario)
        {
            string sql;
            sql = "UPDATE clientes SET nombre_usuario ='" + nombre_usuario + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponNombre(string DNI, string nombre)
        {
            string sql;
            sql = "UPDATE clientes SET nombre ='" + nombre + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponDNI(string DNI)
        {
            string sql;
            sql = "UPDATE clientes SET DNI ='" + DNI + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponTelefono(string DNI, int telefono)
        {
            string sql;
            sql = "UPDATE clientes SET telefono=" + telefono + " WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponFecha(string DNI,string f)
        {
            string sql;
            sql = "UPDATE clientes SET data_naixement= '" + f + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponContrasenya(string DNI, string contrasenya)
        {
            string sql;
            sql = "UPDATE clientes SET contrasenya = '" + contrasenya + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponEmail(string DNI, string email)
        {
            string sql;
            sql = "UPDATE clientes SET email ='" + email+ "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponDireccion(string DNI, string direccion)
        {
            string sql;
            sql = "UPDATE clientes SET direccion_postal ='" + direccion+ "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponNumero_tarjeta(string DNI, string numero_tarjeta)
        {
            string sql;
            sql = "UPDATE clientes SET numero_tarjeta ='" + numero_tarjeta + "' WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int ponGasto(string DNI,double gasto)
        {
            string sql;
            sql = "UPDATE clientes SET gasto =" + gasto + " WHERE DNI='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public DataSet buscarCliente(string nombre_usuario)
        //retorna un DataSet con l 
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT * FROM clientes WHERE nombre_usuario ='" + nombre_usuario + "';";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "clientes");
            return ds;
        }

        public DataSet buscarAdministrador(string nombre_administrador)
        //retorna un DataSet con el administrador en la tabla administradores cuyo nombre es nombre_administrador
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT * FROM administradores WHERE nombre_usuario ='" + nombre_administrador + "';";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "administradores");
            return ds;
        }


        public DataSet GetContrasenya(string nombre_usuario)
        //retorna un DataSet con la contrasenya del usuario con nombre_usuario
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT contrasenya FROM clientes WHERE nombre_usuario ='"+nombre_usuario+"';";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "clientes");
            return ds;
        }


        public int deleteCliente(string DNI)
        {
            string sql;
            sql = "DELETE FROM clientes WHERE DNI ='" + DNI + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }

        public int deleteAdministrador(string nombre)
            //elimina el administrador con nombre_usuario de la tabla administradores
        {
            string sql;
            sql = "DELETE FROM administradores WHERE nombre_usuario ='" + nombre + "';";
            SQLiteCommand command = new SQLiteCommand(sql, cnx);
            int rowsAffected = command.ExecuteNonQuery();
            return rowsAffected;
        }
        public DataSet GetClientesordenadosgasto()
        {
            DbDataAdapter adp;
            DataSet ds = new DataSet();
            string sql;
            sql = "SELECT * FROM clientes order by ;";
            adp = new SQLiteDataAdapter(sql, cnx);
            adp.Fill(ds, "clientes");
            return ds;
        }
   
    }
}
