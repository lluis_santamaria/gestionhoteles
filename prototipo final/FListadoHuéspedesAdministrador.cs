﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class FListadoHuéspedesAdministrador : Form
    {
        CHotel h = new CHotel();

        public FListadoHuéspedesAdministrador(CHotel h)
        {
            this.h = h;
            InitializeComponent();
        }

        
        static string[] ordenar(int ord,string[] nombre)
        //esta función devuelve el vector de strings nombre ordenado ascendente o descendentemente según el valor de ord que le introduzcamos
        {
            
            int i, j, minimum;
            string temporal;
            switch (ord)
            {
                case 0:
                    i = 0;
                    
                    while (i < nombre.Length)
                    {
                        
                        minimum = i;
                        j = i + 1;
                        while (j < nombre.Length)
                        {
                            if (nombre[j].CompareTo(nombre[minimum]) < 0)
                            {
                                minimum = j;
                            }
                            j = j + 1;
                        }
                        temporal = nombre[minimum];
                        nombre[minimum] = nombre[i];
                        nombre[i] = temporal;
                        i = i + 1;
                    }
                    //Console.WriteLine("El vector ordenado ascendentemente es: ");
                    
                    return nombre;

                   

                case -1:

                    int maximum;

                    i = 0;
                    while (i < nombre.Length)
                    {
                        maximum = i;
                        j = i + 1;
                        while (j < nombre.Length)
                        {
                            if (nombre[j].CompareTo(nombre[maximum]) > 0)
                            {
                                maximum = j;
                            }
                            j = j + 1;
                        }
                        temporal = nombre[maximum];
                        nombre[maximum] = nombre[i];
                        nombre[i] = temporal;
                        i = i + 1;
                    }
                    //Console.WriteLine("El vector ordenado descendentemente es: ");

                    return nombre;

                default:
                    return nombre;

                    
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            label1.Text = h.get_identificador();

            m2.ColumnCount = 3;
            m2.RowCount = h.get_matriz_personas().GetLength(0)*h.get_matriz_personas().GetLength(1);
            m2.ColumnHeadersVisible = true;
            m2.RowHeadersVisible = false;
            m2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        m2[0, k].Value = h.get_persona(i, j).get_nombre();
                        m2[1, k].Value = h.get_persona(i, j).get_DNI();
                        m2[2, k].Value = Convert.ToString(h.get_persona(i, j).get_edad());
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //una vez hecho el recorrido le reasignamos el tamaño k a la lista de clientes
            m2.RowCount = k;


            //recorremos la matriz de clientes del hotel una vez mas para dibujar la matriz m3 que representa la ocupacion del mismo
            m3.ColumnCount = h.get_matriz_personas().GetLength(0);
            m3.RowCount = h.get_matriz_personas().GetLength(1);
            m3.ColumnHeadersVisible = false;
            m3.RowHeadersVisible = false;
            m3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            i = 0;
            j = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) == null)
                        m3[i, j].Style.BackColor = Color.Green;
                    else
                        m3[i, j].Style.BackColor = Color.Red;
                    j++;
                }
                j = 0;
                i++;
            }

            //actualizamos el balance en su label correspondiente
            label3.Text = Convert.ToString(h.calcular_balance())+" %";
        }

        private void ordenarListaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void nombreToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void m2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        private void m2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ascendenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] vector_temporal = new string[h.get_MAXi()*h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = h.get_persona(i, j).get_nombre();
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_n con los nombres y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_n = new string[k];
            i = 0;
            while (i < k)
            {
                vector_n[i] = vector_temporal[i];
                i++;
            }

            vector_n = ordenar(0, vector_n);
            //asignacion de DNI y edad en 2 nuevos vectores de DNIs y edades paralelos a vector_n

            string[] vector_DNI = new string[k];
            int[] vector_edad = new int[k];
            i = 0;
            

            while (i < k)
            {
                vector_DNI[i] = h.buscar_cliente_nombre(vector_n[i]).get_DNI();
                vector_edad[i] = h.buscar_cliente_nombre(vector_n[i]).get_edad();
                i++;
            }
            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_n[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = Convert.ToString(vector_edad[i]);
                i++;
            }
        }

        private void descendenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] vector_temporal = new string[h.get_MAXi() * h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = h.get_persona(i, j).get_nombre();
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_n con los nombres y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_n = new string[k];
            
            i = 0;
            while (i < k)
            {
                vector_n[i] = vector_temporal[i];
                i++;
            }

            vector_n = ordenar(-1, vector_n);
            //asignacion de DNI y edad en 2 nuevos vectores de DNIs y edades paralelos a vector_n

            int[] vector_edad = new int[k];
            string[] vector_DNI = new string[k];
            i = 0;

            while (i < k)
            {
                vector_DNI[i] = h.buscar_cliente_nombre(vector_n[i]).get_DNI();
                vector_edad[i] = h.buscar_cliente_nombre(vector_n[i]).get_edad();
                i++;
            }
            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_n[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = Convert.ToString(vector_edad[i]);
                i++;
            }
        }

        private void ascendenteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string[] vector_temporal = new string[h.get_MAXi() * h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = h.get_persona(i, j).get_DNI();
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_DNI con los DNI y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_DNI = new string[k];
            i = 0;
            while (i < k)
            {
                vector_DNI[i] = vector_temporal[i];
                i++;
            }

            vector_DNI = ordenar(0, vector_DNI);
            //asignacion de nombres y edades en 2 nuevos vectores de nombres y edades paralelo a vector_DNI

            string[] vector_n = new string[k];
            int[] vector_edad = new int[k];

            i = 0;
           
            while (i < k)
            {
                vector_n[i] = h.buscar_cliente_DNI(vector_DNI[i]).get_nombre();
                vector_edad[i] = h.buscar_cliente_DNI(vector_DNI[i]).get_edad();
                i++;
            }

            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_n[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = Convert.ToString(vector_edad[i]);
                i++;
            }
        }

        private void descendenteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string[] vector_temporal = new string[h.get_MAXi() * h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = h.get_persona(i, j).get_DNI();
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_DNI con los DNI y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_DNI = new string[k];
            i = 0;
            while (i < k)
            {
                vector_DNI[i] = vector_temporal[i];
                i++;
            }

            vector_DNI = ordenar(-1, vector_DNI);

            //asignacion de nombres y edades en 2 nuevos vectores de nombres y edades paralelo a vector_DNI
            string[] vector_n = new string[k];
            int[] vector_edad = new int[k];
            i = 0;

            while (i < k)
            {
                vector_n[i] = h.buscar_cliente_DNI(vector_DNI[i]).get_nombre();
                vector_edad[i] = h.buscar_cliente_DNI(vector_DNI[i]).get_edad();
                i++;
            }
            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_n[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = Convert.ToString(vector_edad[i]);
                i++;
            }
        }

        private void m3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void edadToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void ascendenteToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //conviene señalar que se crean vectores de strings para almacenar las edades (convertidas de int a string) porque la función ordenar()
            //trabaja únicamente con vectores de strings
            string[] vector_temporal = new string[h.get_MAXi()*h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = Convert.ToString(h.get_persona(i, j).get_edad());
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_edad con los nombres y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_edad = new string[k];
            i = 0;
            while (i < k)
            {
                vector_edad[i] = vector_temporal[i];
                i++;
            }

            vector_edad = ordenar(0, vector_edad);
            //asignacion de DNI y nombre en 2 nuevos vectores de DNIs y nombres paralelos a vector_edad

            string[] vector_DNI = new string[k];
            string[] vector_nombre = new string[k];
            i = 0;
            

            while (i < k)
            {
                vector_DNI[i] = h.buscar_cliente_edad(Convert.ToInt32(vector_edad[i])).get_DNI();
                vector_nombre[i] = h.buscar_cliente_edad(Convert.ToInt32(vector_edad[i])).get_nombre();
                i++;
            }
            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_nombre[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = vector_edad[i];
                i++;
            }
        }

        private void descendenteToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //conviene señalar que se crean vectores de strings para almacenar las edades (convertidas de int a string) porque la función ordenar()
            //trabaja únicamente con vectores de strings
            string[] vector_temporal = new string[h.get_MAXi() * h.get_MAXj()];
            int i = 0, j = 0, k = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) != null)
                    {
                        vector_temporal[k] = Convert.ToString(h.get_persona(i, j).get_edad());
                        k++;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
            //creamos un vector_edad con los nombres y sin posiciones sobrantes para evitar la comparacion de valores null en la funcion ordenar()
            string[] vector_edad = new string[k];
            i = 0;
            while (i < k)
            {
                vector_edad[i] = vector_temporal[i];
                i++;
            }

            vector_edad = ordenar(-1, vector_edad);
            //asignacion de DNI y nombre en 2 nuevos vectores de DNIs y nombres paralelos a vector_edad

            string[] vector_DNI = new string[k];
            string[] vector_nombre = new string[k];
            i = 0;


            while (i < k)
            {
                vector_DNI[i] = h.buscar_cliente_edad(Convert.ToInt32(vector_edad[i])).get_DNI();
                vector_nombre[i] = h.buscar_cliente_edad(Convert.ToInt32(vector_edad[i])).get_nombre();
                i++;
            }
            //mostramos m2 con los valores ordenados
            i = 0;
            while (i < k)
            {
                m2[0, i].Value = vector_nombre[i];
                m2[1, i].Value = vector_DNI[i];
                m2[2, i].Value = vector_edad[i];
                i++;
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }     
    }
}
