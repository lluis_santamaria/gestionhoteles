﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class Form7 : Form
    {
        CHotel hotel = new CHotel();
        CPersona nuevo_cliente = new CPersona();
        int i, j;
        CPilaDeshacer p = new CPilaDeshacer();

        public Form7(CHotel hotel,int i,int j,CPilaDeshacer p)
        {
            InitializeComponent();
            this.hotel = hotel;
            this.i = i;
            this.j = j;
            this.p = p;
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //verificamos si alguno de los texbox está vacío y lo indicamos al usuario
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
                MessageBox.Show("Falta introducir alguno de los datos en el campo correspondiente");
            else
            {
                try
                {
                    int edad = Convert.ToInt32(textBox3.Text);
                    if (edad <= 0)
                        MessageBox.Show("La edad tiene que ser un número entero positivo");
                    else
                    {
                        nuevo_cliente.set_nombre(textBox1.Text);
                        nuevo_cliente.set_DNI(textBox2.Text);
                        nuevo_cliente.set_edad(edad);
                        hotel.asignar(nuevo_cliente, i, j);

                        p.push(nuevo_cliente);
                        Close();
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("La edad tiene que ser un número entero positivo");
                    textBox3.Text = "nº entero > 0";
                }
            }

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
