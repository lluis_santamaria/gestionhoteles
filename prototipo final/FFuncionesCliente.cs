﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FFuncionesCliente : Form
    {
        CCliente cliente_actual = new CCliente();
        CGestion mi_base = new CGestion();
        CListaHoteles ll = new CListaHoteles();
        
        public FFuncionesCliente(CListaHoteles ll,CCliente cliente_actual)
        {           
            InitializeComponent();
            this.cliente_actual = cliente_actual;
            this.ll= ll;
        }

        private void Funciones_cliente_Load(object sender, EventArgs e)
        {
            label1.Text = "¡Hola " + cliente_actual.obtener_nombre_usuario() + "!";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FModificarDatosCliente s = new FModificarDatosCliente(cliente_actual);
            s.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FListadoHotelesCliente r = new FListadoHotelesCliente(ll,cliente_actual);
            r.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mi_base.Iniciar();
            if (mi_base.deleteCliente(cliente_actual.obtener_DNI()) == 1)
            {
                MessageBox.Show(cliente_actual.obtener_nombre_usuario()+" , ha sido dado de baja correctamente. Esperamos verle por aquí de nuevo.");
                mi_base.Cerrar();
                Close();
            }
            else
            {
                MessageBox.Show("Parece que ha habido un error en su baja, "+cliente_actual.obtener_nombre_usuario()+". Tendrá que quedarse.");
                mi_base.Cerrar();
            }
        }
    }
}
