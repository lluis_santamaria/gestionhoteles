﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;


namespace WindowsFormsApplication1
{
    
    public partial class FAutentificarCliente : Form
    {
        CGestion mi_base = new CGestion();
        CCliente client = new CCliente();
        CListaHoteles llist = new CListaHoteles();
        

        public FAutentificarCliente(CListaHoteles l)
        {
            InitializeComponent();
            this.llist = l;
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            mi_base.Iniciar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (TBUsuario.Text == "")
                MessageBox.Show("Su nombre de usuario es extremadamente corto");
            if (TBContrasenya.Text == "")
                MessageBox.Show("No ha introducido contraseña");
            if (TBUsuario.Text != "" && TBContrasenya.Text != "")
            {
                DataSet consulta = mi_base.buscarCliente(TBUsuario.Text);
                if (consulta.Tables["clientes"].Rows.Count == 0)
                    MessageBox.Show("El usuario " + TBUsuario.Text + " no existe en nuestra base de datos. Quizás lo encuentre en Google");
                else
                    if (consulta.Tables["clientes"].Rows[0][5].ToString() == TBContrasenya.Text)
                    {
                        string DNI_titular = consulta.Tables["clientes"].Rows[0][2].ToString();
                        client.asignar_DNI(DNI_titular);
                        string nombre_usuario = consulta.Tables["clientes"].Rows[0][0].ToString();
                        client.asignar_nombre_usuario(nombre_usuario);

                        MessageBox.Show("Bienvenido al sistema, " + TBUsuario.Text + ". Hoy es " + DateTime.Now.ToLongDateString()+".");
                        mi_base.Cerrar();
                        FFuncionesCliente f = new FFuncionesCliente(llist,client);
                        f.ShowDialog();
                        Close();
                    }
                    else
                        MessageBox.Show("Contraseña incorrecta. Siga intentando");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void autentificar_timer_cerrar(object sender, EventArgs e)
        {
            Close();
        }
    }
}
