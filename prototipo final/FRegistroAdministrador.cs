﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FRegistroAdministrador : Form
    {
        CGestion bd = new CGestion();

        public FRegistroAdministrador()
        {
            InitializeComponent();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "3";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "2";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "9";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            LCodigo.Text = LCodigo.Text + "0";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            LCodigo.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            bd.Iniciar();
            int r;

            if (TBNombre.Text == "" || TBContrasenya.Text == "")
                MessageBox.Show("Falta introducir algun dato");
            else
            {
                if (TBContrasenya.Text == TBRepetir.Text)
                {
                    if (LCodigo.Text == "040112")
                    {
                        r = bd.Añadir_administrador(TBNombre.Text, TBContrasenya.Text);
                        if (r == 1)
                        {
                            MessageBox.Show("Ha sido añadido correctamente");
                            Close();
                            bd.Cerrar();
                        }
                        else
                            MessageBox.Show("Se ha producido un error añadiéndolo a usted");
                    }
                    else
                    {
                        MessageBox.Show("No trate de inventarse el código de seguridad");
                    }
                }
                else
                    MessageBox.Show("La contraseña no coincide con la repetición de contraseña. Se nota que tienen que ser iguales");
            }
        }

        private void LCodigo_Click(object sender, EventArgs e)
        {

        }
    }
}
