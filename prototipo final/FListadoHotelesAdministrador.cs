﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class FListadoHotelesAdministrador : Form
    {
        CListaHoteles l = new CListaHoteles();
        CHotel[] lista = new CHotel[10];

        string fichero_abierto = "";
        public FListadoHotelesAdministrador(CListaHoteles l)
        {
            InitializeComponent();
            this.l = l;
        }

        public void inicializar_matriz()
        {
            m1.ColumnCount = 2;
            m1.RowCount = l.obtener_numero();
            m1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0;
            while (i < l.obtener_numero())
            {
                m1[0, i].Value = l.obtener_hotel(i).get_identificador();
                m1[1, i].Value = Convert.ToString(l.obtener_hotel(i).get_balance()) +"%";
                i++;
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = e.RowIndex;

            if (i >= 0)
            //este if sirve para acceder a la forma 3 sólo en caso de clicar
            //en una celda que NO corresponde al encabezado de columna (i == -1)
            {
                FListadoHuéspedesAdministrador f3 = new FListadoHuéspedesAdministrador(l.obtener_hotel(i));
                f3.ShowDialog();
            }
        }


        private void Form2_Load(object sender, EventArgs e)
        {
            inicializar_matriz();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            FAltaHotel f4 = new FAltaHotel(l);
            f4.ShowDialog();
            inicializar_matriz();
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (l.cargar("exemple.txt"))
            {
                case 0:
                    MessageBox.Show("Fichero de ejemplo cargado correctamente.");
                    break;
                case -2:
                    MessageBox.Show("Formato del fichero de ejemplo incorrecto.");
                    break;
                case -1: 
                    MessageBox.Show("Fichero de ejemplo no encontrado.");
                    break;
            }
        }

        private void examinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargar_form2.Title = "Cargar";
            cargar_form2.ShowDialog();
            switch (l.cargar(cargar_form2.FileName))
            {
                case 0:
                    MessageBox.Show("Fichero cargado correctamente.");
                    break;
                case -2:
                    MessageBox.Show("Formato del fichero incorrecto.");
                    break;
                case -1:
                    MessageBox.Show("Fichero no encontrado.");
                    break;
            }
        }

        private void m1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fichero_abierto == "")
            {
                string nombre;
                guardar_fichero.Title = "Guardar como";
                guardar_fichero.ShowDialog();
                //robustez si no da nombre al fichero luego no lo encontrará
                nombre = guardar_fichero.FileName;

                l.guardar(nombre);
                fichero_abierto = nombre;
            }
            else
                l.guardar(fichero_abierto);
            
        }

        private void cargarListaHotelesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string nombre;
            guardar_fichero.Title = "Guardar";
            guardar_fichero.ShowDialog();
            nombre = guardar_fichero.FileName;


            l.guardar(nombre);
            fichero_abierto = nombre;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
