﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FModificarDatosCliente : Form
    {
        CGestion bd = new CGestion();
        CCliente c = new CCliente();

        public FModificarDatosCliente(CCliente c)
        {
            InitializeComponent();
            this.c = c;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int r;

            if (TBUsuario.Text == "" || TBNombre.Text == "" || TBDNI.Text == "" || TBContraseña.Text == "" || CBDia.Text == "" || CBMes.Text == "" || CBAño.Text == "" || TBTarjeta.Text == "" || TBDireccion.Text == "" || TBRepitacontraseña.Text == "")
                MessageBox.Show("Falta introducir algun dato");
            else
            {
                if (TBContraseña.Text == TBRepitacontraseña.Text)
                {
                    try
                    {
                        string fechas= CBDia.Text+"/"+CBMes.Text+"/"+CBAño.Text;

                        r = bd.ponNombre_usuario(c.obtener_DNI(),TBUsuario.Text);
                        r=r+bd.ponNombre(c.obtener_DNI(),TBNombre.Text);
                     

                        r=r+bd.ponDNI(TBDNI.Text);
                        r=r+bd.ponTelefono(c.obtener_DNI(),Convert.ToInt32(TBTelefono.Text));

                        r=r+bd.ponFecha(c.obtener_DNI(),fechas);
                        r=r+bd.ponContrasenya(c.obtener_DNI(),TBContraseña.Text);

                                   
                        r=r+bd.ponEmail(c.obtener_DNI(),TBEmail.Text);
                        r=r+bd.ponDireccion(c.obtener_DNI(),TBDireccion.Text+TBDireccion2.Text);
                        r=r+bd.ponNumero_tarjeta(c.obtener_DNI(),TBTarjeta.Text);



                        if (r == 9)
                        {
                            MessageBox.Show("Ha sido modificado correctamente");
                            Close();
                            bd.Cerrar();
                        }

                        else
                        {
                            MessageBox.Show("Se ha producido un error modificando sus datos");
                            bd.Cerrar();
                        }
                    }

                    catch(FormatException)
                    {
                        MessageBox.Show(TBTelefono.Text+" no es un numero de telefono muy comun.No trate de inventarselo");
                    }
                        
                        
                    
                }
                else
                    MessageBox.Show("La contraseña no coincide con la repetición de contraseña. Se nota que tienen que ser iguales");
            }
               

        }

        
        private void FModificarDatosCliente_Load(object sender, EventArgs e)
        //este método abre la base de datos cuando se carga la forma FModificarDatosCliente_Load
        //y busca el cliente autentificado y muestra sus datos en los TextBox's
        {
            bd.Iniciar();

            DataSet consulta = bd.buscarCliente(c.obtener_nombre_usuario());
            TBUsuario.Text = consulta.Tables["clientes"].Rows[0][0].ToString();
            TBNombre.Text = consulta.Tables["clientes"].Rows[0][1].ToString();
            TBDNI.Text = consulta.Tables["clientes"].Rows[0][2].ToString();
            TBTelefono.Text = consulta.Tables["clientes"].Rows[0][3].ToString();

            string[] vector = new string[3];
            vector=consulta.Tables["clientes"].Rows[0][4].ToString().Split('/');

            CBDia.Text = vector[0];
            CBMes.Text = vector[1];
            CBAño.Text = vector[2];        

            TBEmail.Text= consulta.Tables["clientes"].Rows[0][6].ToString();
            TBDireccion.Text = consulta.Tables["clientes"].Rows[0][7].ToString();
            //falta el direccion 2
            TBTarjeta.Text = consulta.Tables["clientes"].Rows[0][8].ToString();   

            
           


        }

        private void button2_Click(object sender, EventArgs e)
        {
            bd.Cerrar();
            Close();
        }
    }
}
