﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FFuncionesAdministrador : Form
    {
        CListaHoteles lista = new CListaHoteles();
        CAdministrador admin = new CAdministrador();
        CGestion mi_base = new CGestion();

        public FFuncionesAdministrador(CListaHoteles lista, CAdministrador a)
        {
            InitializeComponent();
            this.lista = lista;
            admin = a;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FListadoHotelesAdministrador F2 = new FListadoHotelesAdministrador(lista);
            F2.ShowDialog();
        }

        private void Form9_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FListadoClientesAdministrador f = new FListadoClientesAdministrador();
            f.ShowDialog();

            
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mi_base.Iniciar();
            if (mi_base.deleteAdministrador(admin.obtener_nombre()) == 1)
            {
                MessageBox.Show(admin.obtener_nombre() + " , ha sido dado de baja correctamente. Esperamos verle por aquí de nuevo.");
                mi_base.Cerrar();
                Close();
            }
            else
            {
                MessageBox.Show("Parece que ha habido un error en su baja, " + admin.obtener_nombre() + ". Tendrá que quedarse.");
                mi_base.Cerrar();
            }
        }
    }
}
