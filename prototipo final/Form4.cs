﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class Form4 : Form
    {
        CListaHoteles l = new CListaHoteles();      
        
        public Form4(CListaHoteles l)
        {
            
            this.l = l;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CHotel hotel = new CHotel();
            //verificamos si alguno de los texbox está vacío
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
                MessageBox.Show("Falta introducir alguno de los datos en el campo correspondiente");
            else
            {
                try
                {
                    int habitaciones, pisos;
                    habitaciones = Convert.ToInt32(textBox2.Text);
                    pisos = Convert.ToInt32(textBox3.Text);
                    if (habitaciones > hotel.get_MAXi() || habitaciones <= 0)
                        MessageBox.Show("El número de habitaciones por piso debe ser un número del 1 al " + hotel.get_MAXi());
                    else if (pisos > hotel.get_MAXj() || pisos <= 0)
                        MessageBox.Show("El número de pisos debe ser un número del 1 al " + hotel.get_MAXj());
                    else
                    {
                        CPersona[,] matriz_p = new CPersona[habitaciones, pisos];
                        hotel.set_identificador(textBox1.Text);
                        hotel.set_matriz_personas(matriz_p);
                        hotel.set_balance(hotel.calcular_balance());
                        l.asignar_hotel(hotel, l.obtener_numero());
                        l.asignar_numero(l.obtener_numero() + 1);
                        Close();
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Por favor introduzca un nombre en el primer campo de texto y dos números enteros positivos no superiores a " + hotel.get_MAXi() + " y " + hotel.get_MAXj() + " en los siguientes campos respectivamente.");
                    textBox1.Text = "Aquí el nombre";
                    textBox2.Text = "nº no superior a " + hotel.get_MAXi();
                    textBox3.Text = "nº no superior a " + hotel.get_MAXj();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
