﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;


namespace WindowsFormsApplication1
{
    public partial class FRegistro : Form
    {
        CGestion mi_base = new CGestion();


        public FRegistro()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void FRegistro_Load(object sender, EventArgs e)
        {
            mi_base.Iniciar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int r;

            if (TBUsuario.Text == "" || TBNombre.Text == "" || TBDNI.Text == "" || TBContraseña.Text == "" || CBDia.Text == "" || CBMes.Text == "" || CBAño.Text == "" || TBTarjeta.Text == "" || TBDireccion.Text == "" || TBRepitacontraseña.Text == "")
                MessageBox.Show("Falta introducir algun dato");
            else
            {
                if (TBContraseña.Text == TBRepitacontraseña.Text)
                {
                    try
                    {
                        r = mi_base.Añadir_cliente(TBUsuario.Text, TBNombre.Text, TBDNI.Text, Convert.ToInt32(TBTelefono.Text), CBDia.Text + "/" + CBMes.Text + "/" + CBAño.Text, TBContraseña.Text, TBEmail.Text, TBDireccion.Text + TBDireccion2.Text, TBTarjeta.Text);

                        if (r == 1)
                        {
                            MessageBox.Show("Ha sido añadido correctamente");
                            Close();
                            mi_base.Cerrar();
                        }

                        else
                            MessageBox.Show("Se ha producido un error añadiendolo a usted");
                    }

                    catch
                    {
                        MessageBox.Show(TBTelefono.Text+" no es un numero de telefono muy comun.No trate de inventarselo");
                    }
                        
                        
                    
                }
                else
                    MessageBox.Show("La contraseña no coincide con la repetición de contraseña. Se nota que tienen que ser iguales");
            }
               

        }

        private void CBDia_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
