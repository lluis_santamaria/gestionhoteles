﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class FListadoHotelescliente : Form
    {
        CListaHoteles l = new CListaHoteles();

        public FListadoHotelescliente(CListaHoteles l)

        {
            InitializeComponent();
            this.l = l;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = e.RowIndex;

            if (i >= 0)
            //esta condicion hace que solo se acceda a la forma 8 al clicar en
            //una celda corriente, no en el encabezado de una columna (i == -1)
            {
                Form6 f6 = new Form6(l.obtener_hotel(i));
                f6.ShowDialog();
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            m4.ColumnCount = 2;
            m4.RowCount = l.obtener_numero();
            m4.ColumnHeadersVisible = true;
            m4.RowHeadersVisible = false;
            m4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0;
            while (i < l.obtener_numero())
            {
                m4[0, i].Value = l.obtener_hotel(i).get_identificador();
                m4[1, i].Value = Convert.ToString(l.obtener_hotel(i).get_balance()) +"%";
                i++;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            m4.ColumnCount = 2;
            m4.RowCount = l.obtener_numero();
            m4.ColumnHeadersVisible = true;
            m4.RowHeadersVisible = false;
            m4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0;
            while (i < l.obtener_numero())
            {
                m4[0, i].Value = l.obtener_hotel(i).get_identificador();
                m4[1, i].Value = Convert.ToString(l.obtener_hotel(i).get_balance()) + "%";
                i++;
            }
        }

       
    }
}
