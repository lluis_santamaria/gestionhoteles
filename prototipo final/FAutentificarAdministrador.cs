﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FAutentificarAdministrador : Form
    {
        CListaHoteles llista = new CListaHoteles();
        CGestion mi_base = new CGestion();
        CAdministrador admin = new CAdministrador();
       
        
        public FAutentificarAdministrador(CListaHoteles llista)
        {
            InitializeComponent();
            this.llista = llista;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TBAdministrador.Text == "")
                MessageBox.Show("Su nombre de administrador es extremadamente corto");
            if (TBContrasenya.Text == "")
                MessageBox.Show("No ha introducido contraseña");
            if (TBAdministrador.Text != "" && TBContrasenya.Text != "")
            {
                DataSet consulta = mi_base.buscarAdministrador(TBAdministrador.Text);
                if (consulta.Tables["administradores"].Rows.Count == 0)
                    MessageBox.Show("El administrador " + TBAdministrador.Text + " no existe en nuestra base de datos. Quizás lo encuentre en Google.");
                else
                    if (consulta.Tables["administradores"].Rows[0][1].ToString() == TBContrasenya.Text)
                    {
                        string nombre_admin = consulta.Tables["administradores"].Rows[0][0].ToString();
                        admin.asignar_nombre(nombre_admin);
                        string contrasenya_admin = consulta.Tables["administradores"].Rows[0][1].ToString();
                        admin.asignar_contrasenya(contrasenya_admin);

                        MessageBox.Show("Bienvenido al sistema, " + TBAdministrador.Text + ". Hoy es " + DateTime.Now.ToLongDateString() + ".");
                        mi_base.Cerrar();
                        FFuncionesAdministrador f = new FFuncionesAdministrador(llista,admin);
                        f.ShowDialog();
                        Close();
                    }
                    else
                        MessageBox.Show("Contraseña incorrecta. Siga intentando");

            }
        }

        private void TBContrasenya_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FRegistroAdministrador f = new FRegistroAdministrador();
            f.ShowDialog();
            Close();
        }

        private void FAutentificarAdministrador_Load(object sender, EventArgs e)
        {
            mi_base.Iniciar();
        }
    }
}
