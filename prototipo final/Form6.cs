﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class Form6 : Form
    {
        CHotel h = new CHotel();
        

        public Form6(CHotel h)
        {
            InitializeComponent();
            this.h = h;
            
        }
        CPilaDeshacer p = new CPilaDeshacer();

        public void inicializa_matriz()
        {
            label1.Text = (h.get_identificador());


            m6.ColumnCount = h.get_matriz_personas().GetLength(0);
            m6.RowCount = h.get_matriz_personas().GetLength(1);

            m6.ColumnHeadersVisible = false;
            m6.RowHeadersVisible = false;
            m6.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0, j = 0;
            while (i < h.get_matriz_personas().GetLength(0))
            {
                while (j < h.get_matriz_personas().GetLength(1))
                {
                    if (h.get_persona(i, j) == null)
                        m6[i, j].Style.BackColor = Color.Green;
                    else
                        m6[i, j].Style.BackColor = Color.Red;
                    j++;
                }
                j = 0;
                i++;

            }
            label3.Text = h.get_balance() + "%";
        }
            

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form6_Load(object sender, EventArgs e)
        {
            inicializa_matriz();
            
            
        }

        private void m6_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i,j;
            j=e.RowIndex;
            i=e.ColumnIndex;
            if (m6[i, j].Style.BackColor == Color.Red)
            {
                MessageBox.Show("La habitación está ocupada. Se nota que el color rojo indica ocupado y el verde libre");
            }
            else
            {
                Form7 f7 = new Form7(h,i,j,p);
                f7.ShowDialog();
                inicializa_matriz();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (p.is_empty())
            {
                MessageBox.Show("No se pueden deshacer mas reservas de las que se han hecho");
            }
            else
            {
                
                h.eliminar(p.pop().get_nombre());
                

                inicializa_matriz();
            }
        }
    }
}
