﻿namespace WindowsFormsApplication1
{
    partial class FRegistro
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBUsuario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TBNombre = new System.Windows.Forms.TextBox();
            this.TBDNI = new System.Windows.Forms.TextBox();
            this.TBDireccion = new System.Windows.Forms.TextBox();
            this.TBTelefono = new System.Windows.Forms.TextBox();
            this.TBEmail = new System.Windows.Forms.TextBox();
            this.TBContraseña = new System.Windows.Forms.TextBox();
            this.TBRepitacontraseña = new System.Windows.Forms.TextBox();
            this.TBTarjeta = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.TBDireccion2 = new System.Windows.Forms.TextBox();
            this.CBAño = new System.Windows.Forms.ComboBox();
            this.CBMes = new System.Windows.Forms.ComboBox();
            this.CBDia = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // TBUsuario
            // 
            this.TBUsuario.Location = new System.Drawing.Point(145, 32);
            this.TBUsuario.Name = "TBUsuario";
            this.TBUsuario.Size = new System.Drawing.Size(282, 20);
            this.TBUsuario.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre de usuario:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nombre y apellidos:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "DNI:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(142, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Fecha de nacimiento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Dirección postal:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Teléfono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(171, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Email:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Contraseña:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Repita la contraseña:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 250);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Nº Tarjeta de crédito:";
            // 
            // TBNombre
            // 
            this.TBNombre.Location = new System.Drawing.Point(145, 58);
            this.TBNombre.Name = "TBNombre";
            this.TBNombre.Size = new System.Drawing.Size(282, 20);
            this.TBNombre.TabIndex = 13;
            // 
            // TBDNI
            // 
            this.TBDNI.Location = new System.Drawing.Point(66, 88);
            this.TBDNI.Name = "TBDNI";
            this.TBDNI.Size = new System.Drawing.Size(64, 20);
            this.TBDNI.TabIndex = 14;
            // 
            // TBDireccion
            // 
            this.TBDireccion.Location = new System.Drawing.Point(114, 117);
            this.TBDireccion.Name = "TBDireccion";
            this.TBDireccion.Size = new System.Drawing.Size(313, 20);
            this.TBDireccion.TabIndex = 16;
            // 
            // TBTelefono
            // 
            this.TBTelefono.Location = new System.Drawing.Point(89, 169);
            this.TBTelefono.Name = "TBTelefono";
            this.TBTelefono.Size = new System.Drawing.Size(66, 20);
            this.TBTelefono.TabIndex = 17;
            // 
            // TBEmail
            // 
            this.TBEmail.Location = new System.Drawing.Point(212, 169);
            this.TBEmail.Name = "TBEmail";
            this.TBEmail.Size = new System.Drawing.Size(215, 20);
            this.TBEmail.TabIndex = 18;
            // 
            // TBContraseña
            // 
            this.TBContraseña.Location = new System.Drawing.Point(145, 195);
            this.TBContraseña.Name = "TBContraseña";
            this.TBContraseña.Size = new System.Drawing.Size(282, 20);
            this.TBContraseña.TabIndex = 19;
            this.TBContraseña.UseSystemPasswordChar = true;
            // 
            // TBRepitacontraseña
            // 
            this.TBRepitacontraseña.Location = new System.Drawing.Point(145, 221);
            this.TBRepitacontraseña.Name = "TBRepitacontraseña";
            this.TBRepitacontraseña.Size = new System.Drawing.Size(282, 20);
            this.TBRepitacontraseña.TabIndex = 20;
            this.TBRepitacontraseña.UseSystemPasswordChar = true;
            // 
            // TBTarjeta
            // 
            this.TBTarjeta.Location = new System.Drawing.Point(145, 247);
            this.TBTarjeta.Name = "TBTarjeta";
            this.TBTarjeta.Size = new System.Drawing.Size(282, 20);
            this.TBTarjeta.TabIndex = 21;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 46);
            this.button1.TabIndex = 22;
            this.button1.Text = "Aceptar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(329, 282);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 46);
            this.button2.TabIndex = 23;
            this.button2.Text = "Volver";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TBDireccion2
            // 
            this.TBDireccion2.Location = new System.Drawing.Point(34, 143);
            this.TBDireccion2.Name = "TBDireccion2";
            this.TBDireccion2.Size = new System.Drawing.Size(393, 20);
            this.TBDireccion2.TabIndex = 24;
            // 
            // CBAño
            // 
            this.CBAño.FormattingEnabled = true;
            this.CBAño.Items.AddRange(new object[] {
            "1900",
            "1901",
            "1902",
            "1903",
            "1904",
            "1905",
            "1906",
            "1907",
            "1908",
            "1909",
            "1910",
            "1911",
            "1912",
            "1913",
            "1914",
            "1915",
            "1916",
            "1917",
            "1918",
            "1919",
            "1920",
            "1921",
            "1922",
            "1923",
            "1924",
            "1925",
            "1926",
            "1927",
            "1928",
            "1929",
            "1930",
            "1931",
            "1932",
            "1933",
            "1934",
            "1935",
            "1936",
            "1937",
            "1938",
            "1939",
            "1940",
            "1941",
            "1942",
            "1943",
            "1944",
            "1945",
            "1946",
            "1947",
            "1948",
            "1949",
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993"});
            this.CBAño.Location = new System.Drawing.Point(351, 87);
            this.CBAño.Name = "CBAño";
            this.CBAño.Size = new System.Drawing.Size(76, 21);
            this.CBAño.TabIndex = 27;
            // 
            // CBMes
            // 
            this.CBMes.FormattingEnabled = true;
            this.CBMes.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.CBMes.Location = new System.Drawing.Point(304, 87);
            this.CBMes.Name = "CBMes";
            this.CBMes.Size = new System.Drawing.Size(41, 21);
            this.CBMes.TabIndex = 26;
            // 
            // CBDia
            // 
            this.CBDia.FormattingEnabled = true;
            this.CBDia.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.CBDia.Location = new System.Drawing.Point(257, 88);
            this.CBDia.Name = "CBDia";
            this.CBDia.Size = new System.Drawing.Size(41, 21);
            this.CBDia.TabIndex = 25;
            this.CBDia.SelectedIndexChanged += new System.EventHandler(this.CBDia_SelectedIndexChanged);
            // 
            // FRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 344);
            this.Controls.Add(this.CBAño);
            this.Controls.Add(this.CBMes);
            this.Controls.Add(this.CBDia);
            this.Controls.Add(this.TBDireccion2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TBTarjeta);
            this.Controls.Add(this.TBRepitacontraseña);
            this.Controls.Add(this.TBContraseña);
            this.Controls.Add(this.TBEmail);
            this.Controls.Add(this.TBTelefono);
            this.Controls.Add(this.TBDireccion);
            this.Controls.Add(this.TBDNI);
            this.Controls.Add(this.TBNombre);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TBUsuario);
            this.Name = "FRegistro";
            this.Text = "Formulario de registro - Nuevo usuario";
            this.Load += new System.EventHandler(this.FRegistro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TBNombre;
        private System.Windows.Forms.TextBox TBDNI;
        private System.Windows.Forms.TextBox TBDireccion;
        private System.Windows.Forms.TextBox TBTelefono;
        private System.Windows.Forms.TextBox TBEmail;
        private System.Windows.Forms.TextBox TBContraseña;
        private System.Windows.Forms.TextBox TBRepitacontraseña;
        private System.Windows.Forms.TextBox TBTarjeta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox TBDireccion2;
        private System.Windows.Forms.ComboBox CBAño;
        private System.Windows.Forms.ComboBox CBMes;
        private System.Windows.Forms.ComboBox CBDia;
    }
}