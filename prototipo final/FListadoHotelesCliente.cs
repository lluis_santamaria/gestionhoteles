﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class FListadoHotelesCliente : Form
    {
        CListaHoteles l = new CListaHoteles();
        CCliente c = new CCliente();
        
        

        public FListadoHotelesCliente(CListaHoteles l,CCliente c)

        {
            InitializeComponent();
            this.l = l;
            this.c = c;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = e.RowIndex;

            if (i >= 0)
            //esta condicion hace que solo se acceda a la forma 8 al clicar en
            //una celda corriente, no en el encabezado de una columna (i == -1)
            {
                FInformacionHotel f6 = new FInformacionHotel(l.obtener_hotel(i),c);
                f6.ShowDialog();
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            //activamos el timer que refresca el contenido de las 2 matrices de la forma para que siempre estén actualizadas
            timer1.Enabled = true;
            m4.ColumnCount = 2;
            m4.RowCount = l.obtener_numero();
            m4.ColumnHeadersVisible = true;
            m4.RowHeadersVisible = false;
            m4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0;
            while (i < l.obtener_numero())
            {
                m4[0, i].Value = l.obtener_hotel(i).get_identificador();
                m4[1, i].Value = Convert.ToString(l.obtener_hotel(i).get_balance()) +"%";
                i++;
            }


            //dibujamos la matriz de reservas previo recorrido de cada uno de sus huéspedes (de TODA LA LISTA DE HOTELES)
            mreservas.ColumnCount = 4;
            //inicialmente dibujamos tantas filas como habitaciones hay en el total de hoteles (nº hoteles * nº max pisos/hotel * nº max hab piso
            //mreservas.RowCount = l.obtener_numero() * l.obtener_hotel(0).get_MAXi() * l.obtener_hotel(0).get_MAXj();
            mreservas.ColumnHeadersVisible = true;
            mreservas.RowHeadersVisible = false;
            mreservas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            i = 0;
            int j = 0, k = 0, m = 0;

            while (i < l.obtener_numero())
            {
                while (j < l.obtener_hotel(i).get_matriz_personas().GetLength(0))
                {
                    while (k < l.obtener_hotel(i).get_matriz_personas().GetLength(1))
                    {
                        if (l.obtener_hotel(i).get_persona(j, k) != null)
                        {
                            if (l.obtener_hotel(i).get_persona(j, k).get_DNI_titular() == c.obtener_DNI())
                            {
                                mreservas[0, m].Value = l.obtener_hotel(i).get_identificador();
                                mreservas[1, m].Value = l.obtener_hotel(i).get_persona(j, k).get_nombre();
                                mreservas[2, m].Value = l.obtener_hotel(i).get_persona(j, k).get_DNI();
                                mreservas[3, m].Value = Convert.ToString(l.obtener_hotel(i).get_persona(j, k).get_edad());
                                m++;
                            }
                        }
                        k++;
                    }
                    k = 0;
                    j++;
                }
                j = 0;
                i++;
            }
            //el número de filas a dibujar se determina al final del recorrido, pues sólo en éste punto sabremos
            //cuántas reservas tiene ése cliente determinado (almacenado en la variable iterativa m)   
            mreservas.RowCount = m + 1;
 

        }

        private void timer1_Tick(object sender, EventArgs e)
        //éste timer refresca la matriz m4 que muestra la lista de hoteles y mreservas (lista de reservas del usuario) por si ha habido algun cambio.
        {
            m4.ColumnCount = 2;
            m4.RowCount = l.obtener_numero();
            m4.ColumnHeadersVisible = true;
            m4.RowHeadersVisible = false;
            m4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            int i = 0;
            while (i < l.obtener_numero())
            {
                m4[0, i].Value = l.obtener_hotel(i).get_identificador();
                m4[1, i].Value = Convert.ToString(l.obtener_hotel(i).get_balance()) + "%";
                i++;
            }

            //dibujamos la matriz de reservas previo recorrido de cada uno de sus huéspedes (de TODA LA LISTA DE HOTELES)
            mreservas.ColumnCount = 4;
            //inicialmente dibujamos tantas filas como habitaciones hay en el total de hoteles (nº hoteles * nº max pisos/hotel * nº max hab piso
            mreservas.RowCount = l.obtener_numero() * l.obtener_hotel(0).get_MAXi() * l.obtener_hotel(0).get_MAXj();
            mreservas.ColumnHeadersVisible = true;
            mreservas.RowHeadersVisible = false;
            mreservas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            i = 0;
            int j = 0, k = 0, m = 0;

            while (i < l.obtener_numero())
            {
                while (j < l.obtener_hotel(i).get_matriz_personas().GetLength(0))
                {
                    while (k < l.obtener_hotel(i).get_matriz_personas().GetLength(1))
                    {
                        if (l.obtener_hotel(i).get_persona(j, k) != null)
                        {
                            if (l.obtener_hotel(i).get_persona(j, k).get_DNI_titular() == c.obtener_DNI())
                            {
                                mreservas[0, m].Value = l.obtener_hotel(i).get_identificador();
                                mreservas[1, m].Value = l.obtener_hotel(i).get_persona(j, k).get_nombre();
                                mreservas[2, m].Value = l.obtener_hotel(i).get_persona(j, k).get_DNI();
                                mreservas[3, m].Value = Convert.ToString(l.obtener_hotel(i).get_persona(j, k).get_edad());
                                m++;
                            }
                        }
                        k++;
                    }
                    k = 0;
                    j++;
                }
                j = 0;
                i++;
            }
            //el número de filas a dibujar se determina al final del recorrido, pues sólo en éste punto sabremos
            //cuántas reservas tiene ése cliente determinado (almacenado en la variable iterativa m)
            mreservas.RowCount = m + 1;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void m4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

       
    }
}
