﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestionLib;

namespace WindowsFormsApplication1
{
    public partial class FListadoClientesAdministrador : Form
    {
        CGestion mi_base = new CGestion();
        
        public FListadoClientesAdministrador()
        {
            InitializeComponent();
        }

        private void FListarClientesAdministrador_Load(object sender, EventArgs e)
        {
            mi_base.Iniciar();
            DataSet consulta = mi_base.GetClientesSinContrasenya();
            matriz_clientes.DataSource = consulta;
            matriz_clientes.DataMember = "clientes";
            matriz_clientes.Refresh();
            mi_base.Cerrar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void porGastoRealizadoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void matriz_clientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
