﻿namespace WindowsFormsApplication1
{
    partial class FListadoHuéspedesAdministrador
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.m2 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m3 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenarListaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nombreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ascendenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descendenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dNIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ascendenteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.descendenteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.edadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ascendenteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.descendenteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m3)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m2
            // 
            this.m2.AllowUserToAddRows = false;
            this.m2.AllowUserToDeleteRows = false;
            this.m2.AllowUserToResizeColumns = false;
            this.m2.AllowUserToResizeRows = false;
            this.m2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.m2.Location = new System.Drawing.Point(185, 37);
            this.m2.Name = "m2";
            this.m2.RowHeadersVisible = false;
            this.m2.Size = new System.Drawing.Size(306, 255);
            this.m2.TabIndex = 0;
            this.m2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m2_CellClick);
            this.m2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m2_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "Clientes";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "DNI";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Edad";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // m3
            // 
            this.m3.AllowUserToAddRows = false;
            this.m3.AllowUserToDeleteRows = false;
            this.m3.AllowUserToResizeColumns = false;
            this.m3.AllowUserToResizeRows = false;
            this.m3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.m3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.m3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m3.ColumnHeadersVisible = false;
            this.m3.Location = new System.Drawing.Point(29, 150);
            this.m3.MultiSelect = false;
            this.m3.Name = "m3";
            this.m3.ReadOnly = true;
            this.m3.RowHeadersVisible = false;
            this.m3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m3.Size = new System.Drawing.Size(116, 113);
            this.m3.TabIndex = 1;
            this.m3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m3_CellContentClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(516, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordenarListaToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.archivoToolStripMenuItem.Text = "Lista";
            this.archivoToolStripMenuItem.Click += new System.EventHandler(this.archivoToolStripMenuItem_Click);
            // 
            // ordenarListaToolStripMenuItem
            // 
            this.ordenarListaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nombreToolStripMenuItem,
            this.dNIToolStripMenuItem,
            this.edadToolStripMenuItem});
            this.ordenarListaToolStripMenuItem.Name = "ordenarListaToolStripMenuItem";
            this.ordenarListaToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.ordenarListaToolStripMenuItem.Text = "Ordenar lista por...";
            this.ordenarListaToolStripMenuItem.Click += new System.EventHandler(this.ordenarListaToolStripMenuItem_Click);
            // 
            // nombreToolStripMenuItem
            // 
            this.nombreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendenteToolStripMenuItem,
            this.descendenteToolStripMenuItem});
            this.nombreToolStripMenuItem.Name = "nombreToolStripMenuItem";
            this.nombreToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.nombreToolStripMenuItem.Text = "Nombre";
            this.nombreToolStripMenuItem.Click += new System.EventHandler(this.nombreToolStripMenuItem_Click);
            // 
            // ascendenteToolStripMenuItem
            // 
            this.ascendenteToolStripMenuItem.Name = "ascendenteToolStripMenuItem";
            this.ascendenteToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.ascendenteToolStripMenuItem.Text = "ascendente";
            this.ascendenteToolStripMenuItem.Click += new System.EventHandler(this.ascendenteToolStripMenuItem_Click);
            // 
            // descendenteToolStripMenuItem
            // 
            this.descendenteToolStripMenuItem.Name = "descendenteToolStripMenuItem";
            this.descendenteToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.descendenteToolStripMenuItem.Text = "descendente";
            this.descendenteToolStripMenuItem.Click += new System.EventHandler(this.descendenteToolStripMenuItem_Click);
            // 
            // dNIToolStripMenuItem
            // 
            this.dNIToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendenteToolStripMenuItem1,
            this.descendenteToolStripMenuItem1});
            this.dNIToolStripMenuItem.Name = "dNIToolStripMenuItem";
            this.dNIToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.dNIToolStripMenuItem.Text = "DNI";
            // 
            // ascendenteToolStripMenuItem1
            // 
            this.ascendenteToolStripMenuItem1.Name = "ascendenteToolStripMenuItem1";
            this.ascendenteToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.ascendenteToolStripMenuItem1.Text = "ascendente";
            this.ascendenteToolStripMenuItem1.Click += new System.EventHandler(this.ascendenteToolStripMenuItem1_Click);
            // 
            // descendenteToolStripMenuItem1
            // 
            this.descendenteToolStripMenuItem1.Name = "descendenteToolStripMenuItem1";
            this.descendenteToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.descendenteToolStripMenuItem1.Text = "descendente";
            this.descendenteToolStripMenuItem1.Click += new System.EventHandler(this.descendenteToolStripMenuItem1_Click);
            // 
            // edadToolStripMenuItem
            // 
            this.edadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendenteToolStripMenuItem2,
            this.descendenteToolStripMenuItem2});
            this.edadToolStripMenuItem.Name = "edadToolStripMenuItem";
            this.edadToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.edadToolStripMenuItem.Text = "Edad";
            this.edadToolStripMenuItem.Click += new System.EventHandler(this.edadToolStripMenuItem_Click);
            // 
            // ascendenteToolStripMenuItem2
            // 
            this.ascendenteToolStripMenuItem2.Name = "ascendenteToolStripMenuItem2";
            this.ascendenteToolStripMenuItem2.Size = new System.Drawing.Size(142, 22);
            this.ascendenteToolStripMenuItem2.Text = "Ascendente";
            this.ascendenteToolStripMenuItem2.Click += new System.EventHandler(this.ascendenteToolStripMenuItem2_Click);
            // 
            // descendenteToolStripMenuItem2
            // 
            this.descendenteToolStripMenuItem2.Name = "descendenteToolStripMenuItem2";
            this.descendenteToolStripMenuItem2.Size = new System.Drawing.Size(142, 22);
            this.descendenteToolStripMenuItem2.Text = "Descendente";
            this.descendenteToolStripMenuItem2.Click += new System.EventHandler(this.descendenteToolStripMenuItem2_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(79, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Identificador";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ocupación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(113, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "%";
            // 
            // FListadoClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 324);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m3);
            this.Controls.Add(this.m2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FListadoClientes";
            this.Text = "Listado de clientes";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m3)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView m2;
        private System.Windows.Forms.DataGridView m3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordenarListaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nombreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dNIToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem ascendenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descendenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ascendenteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem descendenteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem edadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ascendenteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem descendenteToolStripMenuItem2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}