﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PeopleLib;

namespace WindowsFormsApplication1
{
    public partial class FBienvenida : Form
    {
        public FBienvenida()
        {
            InitializeComponent();
        }
        
        CListaHoteles lliste = new CListaHoteles();


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FAutentificarAdministrador f9 = new FAutentificarAdministrador(lliste);
            f9.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            FAutentificarCliente f5 = new FAutentificarCliente(lliste);
            f5.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (lliste.cargar("exemple.txt") == 0)
                MessageBox.Show("Fichero predeterminado cargado correctamente");

            else if(lliste.cargar("exemple.txt") == -2)
                MessageBox.Show("Formato incorrecto");

            else 
                MessageBox.Show("Fichero no encontrado");
                
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FRegistro fregistro = new FRegistro();
            fregistro.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Red;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Black;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Red;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Black;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Opción no disponible, el botón 'Invitado' es testimonial.");
        }
    }
}
